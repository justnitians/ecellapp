package app.startups.nitrr.ecell.ecellapp.events.view;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.startups.nitrr.ecell.ecellapp.R;
import app.startups.nitrr.ecell.ecellapp.events.model.EventListProvider;

public class MainActivity extends AppCompatActivity implements EventListActivityInterface {
    ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
    private List<EventsData> EventList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Adapter mAdapter;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new Adapter(EventList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);



        new GetData().execute();




    }
    @Override
    public void loaderVisible(boolean val)
    {

        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        if(val)
        pDialog.show();
        else
            pDialog.dismiss();

    }

    private class GetData extends AsyncTask<Void, Void, Void>
    {
        String jsonStr;
        ProgressDialog pDialog = new ProgressDialog(MainActivity.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

           loaderVisible(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            EventListProvider sh = new EventListProvider();
            jsonStr = sh.JsonfromUrl();
            Log.d("Response: ", "> " + jsonStr);
            try
            {
                JSONObject jsonRootObject = new JSONObject(jsonStr);
                JSONArray jsonArray = jsonRootObject.optJSONArray("events");
                int i=R.drawable.ecell3;
                EventsData eventsData;
                for(int i1=0; i1 < jsonArray.length(); i1++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String name=jsonObject.optString("name").toString();
                    String date=jsonObject.optString("date").toString();
                    String time=jsonObject.optString("time").toString();
                    String venue=jsonObject.optString("venue").toString();
                    String description=jsonObject.optString("description").toString();

                    eventsData=new EventsData(name,i,date,time,venue,description);
                    EventList.add(eventsData);


                }
                mAdapter.notifyDataSetChanged();

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            return null;


        }
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
        }

    }


}
