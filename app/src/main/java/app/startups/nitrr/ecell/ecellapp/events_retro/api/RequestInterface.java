package app.startups.nitrr.ecell.ecellapp.events_retro.api;


import app.startups.nitrr.ecell.ecellapp.events_retro.view.jsonResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Iket on 7/26/2016.
 */
public interface RequestInterface {
    @GET("http://iket0512.esy.es/event.txt")
    Call<jsonResponse> getEvents();

}
