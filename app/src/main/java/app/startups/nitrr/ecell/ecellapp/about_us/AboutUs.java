package app.startups.nitrr.ecell.ecellapp.about_us;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import app.startups.nitrr.ecell.ecellapp.R;


public class AboutUs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
    }
}
