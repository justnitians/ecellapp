package app.startups.nitrr.ecell.ecellapp.events.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.startups.nitrr.ecell.ecellapp.R;

/**
 * Created by Iket on 7/23/2016.
 */
public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    private List<EventsData> EventList;

    public Adapter(List<EventsData>EventList)
    {
    this.EventList=EventList;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView title;


        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.textview1);
            img=(ImageView)view.findViewById(R.id.imgview1);
        }
    }


    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View View = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_event_row, parent, false);

        return new ViewHolder(View);
    }


    public void onBindViewHolder(ViewHolder holder, int position) {

        //view ~~> itemview..Possible cause of error!!


        TextView title=(TextView)holder.itemView.findViewById(R.id.title);
        ImageView img=(ImageView)holder.itemView.findViewById(R.id.imageView);
        title.setText(EventList.get(position).getEventName());
        img.setImageResource(EventList.get(position).getImg());
        }



    public int getItemCount() {
        return EventList.size();
    }



}
